# VehicleClient

El proyecto cliente de registro de vehículos y restricción de movilidad vehícular. Se desarrolló con [Angular CLI](https://github.com/angular/angular-cli) version 13.3.7.

Una versión de prueba puede ser encontrada en https://vehicleapptest.herokuapp.com/

## Ambiente de desarrollo

Se debe usar `npm install` para instalar las dependencias. Una vez realizado esto se puede usa `ng serve` para que node lo maneje como una apliación web. Se lo puede encontrar en `http://localhost:4200/`. En ambiente de desarrollo se encuentra configurado para usar los servicios en localhost:8080


## Build

Se puede usar `ng build` para construir el proyecto. Para el ambiente de produccón se enceuntra configurado para usar los servicios en https://dashboard.heroku.com/apps/vehicleservicetest





