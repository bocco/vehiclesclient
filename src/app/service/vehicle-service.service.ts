import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Vehicle } from '../interfaces/Vehicle';

@Injectable({
  providedIn: 'root'
})
export class VehicleServiceService {
  
  constructor(public http : HttpClient) { }

  urlServices = environment.API_URL;

  saveVehicle(vehicle : Vehicle) : Observable<any>{
    return this.http.post(this.urlServices+"vehicle/save/",vehicle);
  }

  queryVehicle(plateNumber : string) : Observable<Vehicle>{
    return this.http.get<Vehicle>(this.urlServices+"vehicle/plateNumber/"+plateNumber);
  }
  
}
