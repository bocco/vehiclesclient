import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class RestrictionServiceService {

  constructor(public http : HttpClient) { }

  urlServices = environment.API_URL;

  isRestricted(plateNumber : string, date : string) : Observable<any>{
    return this.http.get(this.urlServices+"restriction/plate/"+plateNumber+"/date/"+date);
  }
}
