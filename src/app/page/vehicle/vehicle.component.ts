import { Component, OnInit } from '@angular/core';
import { MenuItem, MessageService } from 'primeng/api';
import { Brand } from 'src/app/interfaces/Brand';
import { Color } from 'src/app/interfaces/Color';
import { Model } from 'src/app/interfaces/Model';
import { Vehicle } from 'src/app/interfaces/Vehicle';
import { VehicleServiceService } from 'src/app/service/vehicle-service.service';

@Component({
  selector: 'app-vehicle',
  templateUrl: './vehicle.component.html',
  styleUrls: ['./vehicle.component.css']
})
export class VehicleComponent implements OnInit {

  plate : string = "";
  modelo : string = "";
  models : Model[];
  brands : Brand[];
  colors : Color[];
  vehicle : Vehicle;
  blockedDocument : boolean = false;
  guardar : boolean = false;
  items : MenuItem[] = [
    {label: 'Registro Vehículos', icon: 'pi pi-fw pi-car', url: '/vehicle'},
    {label: 'Consulta Restricción Movilidad Vehicular', icon: 'pi pi-fw pi-calendar-times', url:'/restriction'},
  ]
  activeItem : MenuItem = this.items[0]
  constructor(private vehicleService : VehicleServiceService,
              private messageService : MessageService ) { 
    this.brands = [
      {name: 'KIA' , code : 'KIA'},
      {name: 'VOLKSWAGEN' , code : 'VOLKSWAGEN'},
      {name: 'CHEVROLET' , code : 'CHEVROLET'},
      {name: 'MAZDA' , code : 'MAZDA'},
      {name: 'GREAT WALL' , code : 'GREAT WALL'},
    ];

    this.models = [
      {name: 'MICRO' , code : 'MICRO'},
      {name: 'SEDAN' , code : 'SEDAN'},
      {name: 'CUV' , code : 'CUV'},
      {name: 'SUV' , code : 'SUV'},
      {name: 'PICK UP' , code : 'PICK UP'},
    ]

    this.colors = [
      {name: 'ROJO' , code : 'ROJO'},
      {name: 'AZUL' , code : 'AZUL'},
      {name: 'VERDE' , code : 'VERDE'},
      {name: 'NEGRO' , code : 'NEGRO'},
      {name: 'BLANCO' , code : 'BLANCO'},
    ]
    this.vehicle = new Vehicle();
  }

  ngOnInit(): void {

  }

  initializeVehicle(){
    this.vehicle = new Vehicle();
    this.guardar=false;
  }

  saveVehicle(){
    this.blockedDocument  = true;
    this.guardar=true;
    this.vehicleService.saveVehicle(this.vehicle).subscribe(
      data =>{
        this.blockedDocument=false;
        let message = "Vehículo guardado con éxito.";
        if(data && data.message){
          message = data.message;
        }
        this.messageService.add({severity:'info',summary:'Información',detail: message});
        
      },
      error =>{
        this.blockedDocument=false;
        this.guardar=false;;
        let errorMessage = "Existe un problema con los servidores. Intentelo después";
        if(error.error && error.error.message){
          errorMessage = error.error.message;
        }
        this.messageService.add({severity:'error',summary:'Error',detail: errorMessage});
      }
    );
  }

}
