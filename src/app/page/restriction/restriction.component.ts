import { Component, OnInit } from '@angular/core';
import { RestrictionServiceService } from 'src/app/service/restriction-service.service';
import {MenuItem, MessageService} from 'primeng/api';
import { VehicleServiceService } from 'src/app/service/vehicle-service.service';
import { Vehicle } from 'src/app/interfaces/Vehicle';

@Component({
  selector: 'app-restriction',
  templateUrl: './restriction.component.html',
  styleUrls: ['./restriction.component.css'
]
})
export class RestrictionComponent implements OnInit {
  
  date : Date = new Date();
  maxDate : Date = new Date();
  plateNumber : string = "";
  blockedDocument : boolean = false;
  displayModal : boolean = false;
  vehicle : Vehicle;
  message : string = "";
  items : MenuItem[] = [
    {label: 'Registro Vehículos', icon: 'pi pi-fw pi-car', url: '/vehicle'},
    {label: 'Consulta Restricción Movilidad Vehicular', icon: 'pi pi-fw pi-calendar-times', url:'/restriction'},
  ]
  activeItem : MenuItem = this.items[1]
  constructor(private restrictionService : RestrictionServiceService,
              private messageService : MessageService,
              private vehicleService : VehicleServiceService          
    ) { 
      this.vehicle = new Vehicle();
    this.initializeTime();
  }
  
  ngOnInit(): void {
  }

  checkRestriction(){
    this.blockedDocument = true;
    this.vehicleService.queryVehicle(this.plateNumber).subscribe(
      data => {
          if(data){
            this.getMobilityRestriction(this.plateNumber,this.formatDate(this.date));
            this.vehicle = data;
          }
          else {
            this.vehicle = new Vehicle();
            this.blockedDocument = false;
            this.messageService.add({severity:'info',summary:'Información',detail: "No se ha registrado el vehículo"})
          }
        }
      ,
      err => {
        this.blockedDocument = false;
        let errorMessage = "Existe un problema con los servidores. Intentelo después"
        if(err.error && err.error.message){
          errorMessage = err.message
        }
        this.messageService.add({severity:'error',summary:'Error',detail: errorMessage});
      }
    )
    
  }


  getMobilityRestriction(plateNumber : string, date :string ){
    this.restrictionService.isRestricted(plateNumber, date)
    .subscribe(data =>{
      this.blockedDocument=false;
      if(data==true){
        this.message = "El vehículo de placa "+this.plateNumber+" presenta restricción"+ 
        " de movilidad en el horario seleccionado.";
      }
      else{
        this.message = "El vehículo de placa "+this.plateNumber+" NO presenta restricción"+ 
        " de movilidad en el horario seleccionado."+"\nDatos de Vehículo:";
      }
      this.showModalDialog();
    },
      err=>{
        this.blockedDocument=false;
        let errorMessage = "Existe un problema con los servidores. Intentelo después"
        
        if(err.error && err.error.message){
          errorMessage = err.message
        }
        
        this.messageService.add({severity:'error',summary:'Error',detail: errorMessage});
      
      }
    );
  }
  

  initializeTime(){
    this.date.setHours(0);
    this.date.setMinutes(0);
    this.date.setSeconds(0);
    this.date.setMilliseconds(0);
  }

  plateNumberIsEmpty(){
    if(this.plateNumber===""){
        return true;
    }
    return false;
  }

  disableButton(){
    if(this.plateNumberIsEmpty())
      return true;
    if(this.date==null)
      return true;

    return false;  
      
  }

  showModalDialog() {
    this.displayModal = true;
  }

  formatDate(date : Date){
    let day = ('0' + date.getDate()).slice(-2);
    let month = ('0' + (date.getMonth() + 1)).slice(-2);
    let year = date.getFullYear();
    let hours = ('0' + date.getHours()).slice(-2);
    let minutes = ('0' + date.getMinutes()).slice(-2);
    let seconds = ('0' + date.getSeconds()).slice(-2);
    let time = day+"-"+month+"-"+year+" "+hours+":"+minutes;
    return time;
  }

}
