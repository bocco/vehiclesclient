import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { RestrictionComponent } from './page/restriction/restriction.component';
import { VehicleComponent } from './page/vehicle/vehicle.component';

const routes: Routes = [
  { path: 'vehicle', component: VehicleComponent },
  { path: 'restriction', component: RestrictionComponent },
  { path: '', redirectTo: 'vehicle', pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
