export class Vehicle{
    idVehicle : number;
    brand : string;
    chassisNumber : string;
    color : string;
    model : string;
    plateNumber : string;

    constructor(){
        this.idVehicle = 0;
        this.brand = "";
        this.chassisNumber = "";
        this.color = "";
        this.model = "";
        this.plateNumber = "";
    }
}