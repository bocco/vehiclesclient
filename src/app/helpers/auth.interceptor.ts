import { HTTP_INTERCEPTORS, HttpEvent, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpHandler, HttpRequest } from '@angular/common/http';
import { Observable } from 'rxjs';

 const TOKEN_HEADER_KEY = 'Authorization';      
 const TOKEN_KEY = 'UGFuZHkyMDIyOlNhbHZlTGFSZWluYQ==';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {
  constructor() { }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    let authReq = req;
    authReq = req.clone({ headers: req.headers.set(TOKEN_HEADER_KEY, 'Basic ' + TOKEN_KEY) });
    return next.handle(authReq);
  }

  

}

export const authInterceptorProviders = [
  { provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true }
];