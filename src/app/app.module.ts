import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { authInterceptorProviders } from './helpers/auth.interceptor';
import { RestrictionComponent } from './page/restriction/restriction.component';
import { VehicleComponent } from './page/vehicle/vehicle.component';
import {CalendarModule} from 'primeng/calendar';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {InputMaskModule} from 'primeng/inputmask';
import {TabMenuModule} from 'primeng/tabmenu';
import {ToastModule} from 'primeng/toast';
import {BlockUIModule} from 'primeng/blockui';
import {DialogModule} from 'primeng/dialog';
import {MessageService} from 'primeng/api';
import {DropdownModule} from 'primeng/dropdown';
import {TooltipModule} from 'primeng/tooltip';
import {InputTextModule} from 'primeng/inputtext';
@NgModule({
  declarations: [
    AppComponent,
    RestrictionComponent,
    VehicleComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    CalendarModule,
    BrowserAnimationsModule,
    InputMaskModule,
    TabMenuModule,
    ToastModule,
    BlockUIModule,
    DialogModule,
    DropdownModule,
    TooltipModule,
    InputTextModule
  ],
  providers: [authInterceptorProviders,
              MessageService],
  bootstrap: [AppComponent]
})
export class AppModule { }
